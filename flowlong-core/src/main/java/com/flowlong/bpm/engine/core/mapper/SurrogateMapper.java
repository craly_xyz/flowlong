package com.flowlong.bpm.engine.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flowlong.bpm.engine.entity.Surrogate;

public interface SurrogateMapper extends BaseMapper<Surrogate> {

}
