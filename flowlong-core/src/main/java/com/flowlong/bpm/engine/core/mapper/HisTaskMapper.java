package com.flowlong.bpm.engine.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flowlong.bpm.engine.entity.HisTask;

public interface HisTaskMapper extends BaseMapper<HisTask> {

}
