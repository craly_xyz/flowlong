/* Copyright 2023-2025 jobob@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package test.mysql.query;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.flowlong.bpm.engine.core.mapper.TaskActorMapper;
import com.flowlong.bpm.engine.entity.TaskActor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.mysql.MysqlTest;

import java.util.List;

/**
 * 查询任务参与者
 */
public class TestQueryTaskActors extends MysqlTest {
    @Autowired
    private TaskActorMapper taskActorMapper;

    @Test
    public void test() {
        Long taskId = 0L;

        // 方式一 取 queryService 查询
        List<TaskActor> byQueryService = flowLongEngine.queryService().getTaskActorsByTaskId(taskId);
        System.out.println("QueryService查询 = " + byQueryService);

        // 方式二 注入 Mapper 查询
        List<TaskActor> byMapper = taskActorMapper.selectList(Wrappers.<TaskActor>lambdaQuery().eq(TaskActor::getTaskId, taskId));
        System.out.println("Mapper查询 = " + byMapper);
    }
}