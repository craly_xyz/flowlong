
<img src="https://foruda.gitee.com/images/1678108376357428538/6b12d4d1_12260.png" alt="flowlong" width="99px" height="118px">

# 项目介绍
FlowLong 飞龙  :tw-1f409: 工作流

特别说明 `flowlong 是基于 snakerflow 重构的` 更适合中国人的国产工作流引擎 。

名字由来 `snakerflow 蛇 （重构进化） flowlong 龙` 中文名 `飞龙` 在天美好愿景！

# 贡献力量

- [运行单元测试](https://gitee.com/aizuda/flowlong/wikis/%E8%BF%90%E8%A1%8C%E5%8D%95%E5%85%83%E6%B5%8B%E8%AF%95)
- PR 请参考现在代码规范注释说明

# 使用文档

[演示前端 flowlong-web](https://gitee.com/aizuda/flowlong-web)


